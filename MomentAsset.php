<?php

namespace azbuco\moment;

use yii\web\AssetBundle;

class MomentAsset extends AssetBundle
{

    public $sourcePath = '@bower/moment';
    public $js = [
        'min/moment-with-locales.min.js',
    ];
    public $publishOptions = [
        'forceCopy' => YII_DEBUG,
    ];

    public function init()
    {
        parent::init();

        \Yii::$app->view->registerJs(';moment.locale("' . \Yii::$app->language . '");');

    }

}
